=== Store Press ===
Contributors: sparklewpthemes
Tags: one-column, two-columns, right-sidebar, left-sidebar, custom-header, custom-background, custom-menu, translation-ready, featured-images, theme-options, custom-logo, e-commerce, footer-widgets
Requires at least: 5.2
Tested up to: 5.9.2
Requires PHP: 7.0
Stable tag: 1.0.3
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Store Press is a clean user-friendly and feature-rich best free eCommerce child WordPress themes. The themes designed and developed especially for online shopping, eCommerce store websites. Store Press is a versatile and highly customizable free WordPress themes that allow you to make your own unique and professional website you have always wanted. Store Press is one of the most accessible eCommerce online store WordPress themes which can easily accommodate all type of users with no coding skills to advanced or normal web developers.

== Description ==

Store Press is a clean user-friendly and feature-rich best free eCommerce Sparkle Store child WordPress themes. This themes is designed and developed especially for online shopping, eCommerce store websites. This is a versatile and highly customizable free Sparkle store Child WordPress themes that allow you to make your own unique and professional website you have always wanted. Store Press is one of the most accessible eCommerce online store WordPress themes which can easily accommodate all type of users with no coding skills to advanced or normal web developers. This theme includes excellent features for eCommerce website and practices of all type of eCommerce business, themes have included more advanced features like one-click demo data import, webpage layout, preloader, advanced theme color, customizer based theme options, page builder-friendly design, individual page &  post layout options also store press is fully compatible with WooCommerce and some other external plugins like YITH WooCommerce Wishlist, YITH WooCommerce Quick View, WOOF – Products Filter for WooCommerce, WooCommerce Variation Swatches, Jetpack, Contact Form 7 and many more plugins. if you face any problem while using our theme, you can refer to our theme documentation (http://docs.sparklewpthemes.com/sparklestore/) or contact our friendly support team (https://sparklewpthemes.com/support/) or Check demo at http://demo.sparklewpthemes.com/sparklestore/store-press/ and Read theme details at https://sparklewpthemes.com/wordpress-themes/store-press


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports WooCommerce and some other external plugins like YITH WooCommerce Wishlist, YITH WooCommerce Quick View, WOOF – Products Filter for WooCommerce, WooCommerce Variation Swatches, Jetpack, Contact Form 7 and many more plugins.

= Where can I find theme all features ? =

You can check our Theme features at https://demo.sparklewpthemes.com/sparklestore/store-press/.

= Where can I find theme demo? =

You can check our Theme Demo at https://sparklewpthemes.com/wordpress-themes/store-press/


== Translation ==

Store Press theme is translation ready.


== Copyright ==

Store Press WordPress Theme is child theme of SparkleStore, Copyright 2017 Sparkle Themes.
Store Press is distributed under the terms of the GNU GPL

SparkleStore WordPress Theme, Copyright (C) 2018 Sparkle Themes.
SparkleStore is distributed under the terms of the GNU GPL

== Credits ==

	Images used in screenshot

	* Products Image - https://pxhere.com/en/photo/1365254
	License: CCO (https://pxhere.com/en/license)

	* Products Image - https://pxhere.com/en/photo/627715
	License: CCO (https://pxhere.com/en/license)

	* Products Image - https://pxhere.com/en/photo/1059478
	License: CCO (https://pxhere.com/en/license)

	* Products Image - https://pxhere.com/en/photo/558021
	License: CCO (https://pxhere.com/en/license)
	
	* Products Image - https://pxhere.com/en/photo/635897
	License: CCO (https://pxhere.com/en/license)

	* Products Image - https://pxhere.com/en/photo/883504
	License: CCO (https://pxhere.com/en/license)


== Changelog ==
= 1.0.5 4th April 2023 = 
* WordPress 6.2 Compatible

= 1.0.4 18th March 2022 = 
* WordPress 5.9.2 Compatible
= 1.0.3 10th Feb 2022 = 
** Responsive Issue Fixed
** Top Header Hiddend for mobile

= 1.0.1/2 30th Jan 2022 = 
** WordPress 5.9 Compatible
** Header Contact Info Added
** Minor Issue on Mobile Fixed

= 1.0.0 18th Jan 2022 =
 ** Initial submit theme on wordpress.org trac.